<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProsesController extends Controller
{
    function create(){
        return view('create021190095');
    }

    function proses(Request $request){
        $data = array();
        $data ['npm'] = $request->npm;
        $data ['nama'] = $request->nama;
        $data ['ps'] = $request->ps;
        $data ['nohp'] = $request->nohp;
        $data ['ttl'] = $request->ttl;
        $data ['jk'] = $request->jk;
        $data ['agama'] = $request->agama;


        return view('view021190095', ['data' => $data]);

    }
}
